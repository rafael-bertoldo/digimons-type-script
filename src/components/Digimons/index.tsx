import { IDigimon } from "../../interfaces/digimon";
import CardDigimon from "../CardDigimon";

interface IDigimonProps {
  digimons: IDigimon[];
  isFavorite?: boolean;
}

const Digimons = ({ digimons, isFavorite = false }: IDigimonProps) => {
  return (
    <>
      {digimons.map((digimon, index) => (
        <CardDigimon digimon={digimon} key={index} isFavorite={isFavorite} />
      ))}
    </>
  );
};

export default Digimons;
