import "./App.css";
import { useFavoriteDigimons } from "./providers/FavoriteDigimons";
import { IDigimon } from "./interfaces/digimon";
import { Container, FavoritesList, List } from "./styles";
import { useEffect, useState } from "react";
import Digimons from "./components/Digimons";

function App() {
  const { favorites } = useFavoriteDigimons();

  const [digimons, setDigimons] = useState<IDigimon[]>([]);
  const [error, setError] = useState<string>("");

  useEffect(() => {
    fetch("https://digimon-api.vercel.app/api/digimon")
      .then((r) => r.json())
      .then((r) => setDigimons([...r]))
      .catch(() => setError("Algo deu Errado"));
  }, []);
  return (
    <div className="App">
      <header className="App-header">
        <Container>
          <h1>Your Favorites Digimons:</h1>
          <FavoritesList>
            <Digimons digimons={favorites} isFavorite={true}></Digimons>
          </FavoritesList>
          <h2>All digimons List:</h2>
          <List>
            <Digimons digimons={digimons}></Digimons>
          </List>
        </Container>
      </header>
    </div>
  );
}

export default App;
